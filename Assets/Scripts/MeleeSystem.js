#pragma strict

var TheDammage : int = 50;
var Distance : float;
var MaxDistance : float = 1.5;
var TheSystem : Transform;
var hitSound1 : AudioClip;
var missSound1 : AudioClip;
function Update ()
{
    if (Input.GetButtonDown("Fire1"))
    {
        //Attack Animation

		//Debug.Log("THis object: " + gameObject.name);
		if (gameObject.name == "Mace")
		{
            animation.Play("Attackmace");
        }
        if (gameObject.name == "Sword")
		{
            animation.Play("Attacksword");
        }
    }
        
    if (animation.isPlaying == false)
    {
        if (gameObject.name == "Mace")
        {
        animation.CrossFade("Idlemace");
    	}
    	if (gameObject.name == "Sword")
    	{
    		animation.CrossFade("Idlesword");
    	}
    }
    if (Input.GetKey (KeyCode.LeftShift))
    {
   		if (gameObject.name == "Mace")
		{
           animation.CrossFade("Sprintmace");
    	}
    	if (gameObject.name == "Sword")
    	{
    		animation.CrossFade("Sprintsword");
    	}

    }
    if (Input.GetKeyUp (KeyCode.LeftShift))
    {
        if (gameObject.name == "Mace")
        {
        	animation.CrossFade("Idlemace");
    	}
    	if (gameObject.name == "Sword")
    	{
    		animation.CrossFade("Idlesword");
    	}
    }
}
function AttackDammage()
{
    var hit : RaycastHit;
    if (Physics.Raycast (TheSystem.transform.position, TheSystem.transform.TransformDirection(Vector3.forward), hit))
    {
        Distance = hit.distance;
        if (Distance < MaxDistance)
        {
            audio.PlayOneShot (hitSound1);
            hit.transform.SendMessage("ApplyDammage", TheDammage, SendMessageOptions.DontRequireReceiver);
        }
        else 
        {
        	audio.PlayOneShot (missSound1);
        }

    }
}