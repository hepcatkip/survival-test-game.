#pragma strict
var monster: GameObject;
var Text: GUIText;
var randomPositions: Transform[];
var numberOfMonstersToSpawn: int = 30;

function Start () {
   
      var i: int = 0;
      while(i < numberOfMonstersToSpawn){
         var spawnIndex: int = Random.Range(0, randomPositions.Length);
         Instantiate(monster, randomPositions[spawnIndex].position, randomPositions[spawnIndex].rotation); 
         i++;
      }
   
}

function Update () {

}