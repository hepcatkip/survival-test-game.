var Distance;
var Target : Transform;
var lookAtDistance = 25.0;
var attactRange = 15.0;
var kill = 4.0;
var moveSpeed = 5.0;
var Damping = 6.0;
var theRender : Transform;
var controller : CharacterController;
var Health = 100;
//var Death = false;
var gravity : float = 80.0;
private var moveDirection : Vector3 = Vector3.zero;
var myAnimation : Animator;
var lastValidPosition : Vector3;
var velBack:float = 120;
var verticalSpeed = 0.0;
var MaxDistance = 1.5;
var TheDammage = 10;
var hitSound1 : AudioClip;
var missSound1 : AudioClip;

function Start()
{
     lastValidPosition = transform.position;  //Hopefully the starting point is valid!
}

function LateUpdate() {
    //if(transform.position.y >= .1 || transform.position.y < 0)
      //   transform.position = lastValidPosition;
    //else
      //   lastValidPosition = transform.position;
}

function Update () 
{
    Distance = Vector3.Distance(Target.position, transform.position);

    if (Distance < lookAtDistance)
    {
        theRender.renderer.material.color = Color.yellow;
        myAnimation.SetBool("run", false);
        myAnimation.SetBool("yells", true);
        //myAnimation.SetBool("Attacking", false);

        lookAt();
    }
    if (Distance > lookAtDistance)
    {
     		myAnimation.SetBool("run", false);
     		myAnimation.SetBool("yells", false);
     		//myAnimation.SetBool("Attacking", false);
            theRender.renderer.material.color = Color.green;
    }
    if (Distance < attactRange && Distance > kill)
    {
        theRender.renderer.material.color = Color.red;
        myAnimation.SetBool("yells", false);
        //myAnimation.SetBool("Attacking", false);
        attack ();
    }
    if (Distance <= kill)
    {
        theRender.renderer.material.color = Color.black;
        myAnimation.SetBool("run", false);
        myAnimation.SetBool("yells", false);
        if (myAnimation.GetBool("Attacking") == false)
        {
        Debug.Log("We Have Attacking True");
        myAnimation.SetBool("Attacking", true);
        swipe();
        }
        else
        {
        //Debug.Log("We Have Attacking false");
        }
    }
    if (Health >0)
    {
    myAnimation.SetBool("Hit", false);
    }
    
    if (Health <= 0)
    {
        Dead();
    }   
}
function lookAt ()
{
    var rotation = Quaternion.LookRotation(Target.position - transform.position);
    rotation.x = 0;
    transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * Damping);
}

function attack ()
{
	if (myAnimation.GetBool("Death") == false || myAnimation.GetBool("Hit") == false)
	{
		myAnimation.SetBool("run", true);
		if (myAnimation.gravityWeight == 1)
    	{
    	//Debug.Log("We Have gravity");
    	}
    	ApplyGravity();
    	transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
    	moveDirection = transform.forward;
		moveDirection *= moveSpeed;
	
		moveDirection.y -= gravity * Time.deltaTime;
		controller.SimpleMove(moveDirection * Time.deltaTime);
		moveDirection.y -= gravity * Time.deltaTime;
		
	}
}
function Dead()
{
    myAnimation.SetBool("Death", true);
    //if (animation.isPlaying == false)
    Destroy (gameObject, 3);
}

function ApplyDammage (TheDammage: int)
{
		myAnimation.SetBool("Hit", true);
        Health -= TheDammage;
        //myAnimation.SetBool("Hit", false);
        //moveDirection = transform.forward * -1;
		//moveDirection *= moveSpeed * TheDammage;
 		
 	    //var dir = (collider.transform.position - transform.position).normalized;
 
        //var characterMotor = collider.GetComponent(CharacterMotor);
        //var characterController = collider.GetComponent(CharacterController);
 
        //characterMotor.SetVelocity(dir*velBack);
 		
 		//moveDirection = Vector3(0, 10,-100);
        //moveDirection = transform.TransformDirection(moveDirection);
 		//moveDirection.y -= gravity * Time.deltaTime;
		//controller.Move(moveDirection * Time.deltaTime);
    	
    	//myself.verticalSpeed = 5;
}
function OnExternalVelocity (vel: float)
{

}
function ApplyGravity()
{ 
    verticalSpeed -= gravity * Time.deltaTime; 
}
function swipe()
{
    var hit : RaycastHit;
    if (Physics.Raycast (transform.position, transform.TransformDirection(Vector3.forward), hit))
    {
        Distance = hit.distance;
        if (Distance < MaxDistance)
        {
            audio.PlayOneShot (hitSound1);
            //Invoke(WaitForSeconds, 2.0);
            hit.transform.SendMessage("ApplyDammage", TheDammage, SendMessageOptions.DontRequireReceiver);
        	Debug.Log("We Have  if");
        	Invoke("Boo", 2.07);

        }
        else 
        {
        	Debug.Log("We Have Else");
        	Invoke("Boo", 2.07);
        	audio.PlayOneShot (missSound1);
        }
   }
   else
   {
   Debug.Log("We Have Error");
   Invoke("Boo", .5);
   }
}

function Boo()
    {
    Debug.Log("We Have Boo");
    myAnimation.SetBool("Attacking", false);
    }     